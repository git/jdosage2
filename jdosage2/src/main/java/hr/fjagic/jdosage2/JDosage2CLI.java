/* 
 * Copyright (c) 2012 Fran Jagić
 * All rights reserved.
 * 
 * This file is part of JDosage2.
 * 
 * JDosage2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * JDosage2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with JDosage2.  If not, see <http://www.gnu.org/licenses/>.
 */
package hr.fjagic.jdosage2;

import hr.fjagic.jdosage2.webcomic.Webcomic;

import java.util.Collection;
import java.util.TreeSet;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.OptionGroup;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

/**
 * Command line interface for JDosage2.
 * 
 * Future versions will include parallel downloads, date/counter generated
 * webcomics and a GUI.
 * 
 * @author Fran Jagi&#263;
 * @since 0.5
 */
public class JDosage2CLI {

	// cli options
	private static final String CLI_LIST = "list";
	private static final String CLI_DOWNLOAD = "download";
	private static final String CLI_WEBCOMIC_FILTER = "webcomic";
	private static final String CLI_GROUP_FILTER = "group";
	// TODO set as system property or webcomic property
	private static final String CLI_IGNORE_MISSING = "ignoremissing";
	private static final String CLI_IGNORE_EXISTING = "ignoreexisting";

	/**
	 * Command line interface for JDosage2.
	 * 
	 * @param args
	 *            command line parameters
	 * @throws ClassNotFoundException
	 *             if selected logger is unavailable
	 */
	@SuppressWarnings("static-access")
	public static void main(String[] args) throws ClassNotFoundException {
		// logger initialization
		freemarker.log.Logger.selectLoggerLibrary(freemarker.log.Logger.LIBRARY_SLF4J);

		// cli option parsing
		Option download = OptionBuilder.withLongOpt(CLI_LIST)
				.withDescription("List webcomics matching the optional filename and group filter").create('l');
		Option list = OptionBuilder.withLongOpt(CLI_DOWNLOAD)
				.withDescription("Download webcomics matching the optional filename and group filter").create('d');
		Option webcomicFilter = OptionBuilder.withLongOpt(CLI_WEBCOMIC_FILTER)
				.withDescription("Select webcomics by definition filename").hasArgs().withArgName("filename(s)")
				.create('w');
		Option groupFilter = OptionBuilder.withLongOpt(CLI_GROUP_FILTER).withDescription("Select webcomics by group")
				.hasArgs().withArgName("group(s)").create('g');
		Option ignoreExisting = OptionBuilder.withLongOpt(CLI_IGNORE_EXISTING)
				.withDescription("Don't stop the download when an existing comic is found").create('e');
		Option ignoreMissing = OptionBuilder.withLongOpt(CLI_IGNORE_MISSING)
				.withDescription("Don't stop the download when no comics are found on a page").create('m');

		Options options = new Options();
		OptionGroup group = new OptionGroup();
		group.addOption(list);
		group.addOption(download);
		group.setRequired(true);
		options.addOptionGroup(group);
		options.addOption(webcomicFilter);
		options.addOption(groupFilter);
		options.addOption(ignoreExisting);
		options.addOption(ignoreMissing);

		try {
			CommandLine commandLine = new BasicParser().parse(options, args);

			Configuration config = Configuration.getInstance();
			WebcomicManager wcm = new WebcomicManager(config);

			Collection<Webcomic> webcomics;
			if (!commandLine.hasOption(CLI_WEBCOMIC_FILTER) && !commandLine.hasOption(CLI_GROUP_FILTER)) {
				webcomics = wcm.getAllWebcomics();
			} else {
				webcomics = new TreeSet<Webcomic>();
				if (commandLine.hasOption(CLI_WEBCOMIC_FILTER)) {
					String[] filenames = commandLine.getOptionValues(CLI_WEBCOMIC_FILTER);
					webcomics.addAll(wcm.getWebcomicsByFilename(filenames));
				}
				if (commandLine.hasOption(CLI_GROUP_FILTER)) {
					String[] groups = commandLine.getOptionValues(CLI_GROUP_FILTER);
					webcomics.addAll(wcm.getWebcomicsByGroup(groups));
				}
			}

			if (commandLine.hasOption(CLI_LIST)) {
				for (Webcomic webcomic : webcomics) {
					System.out.println(webcomic.getName());
					// TODO list groups, progress
				}
			} else if (commandLine.hasOption(CLI_DOWNLOAD)) {
				if (commandLine.hasOption(CLI_IGNORE_EXISTING)) {
					config.setIgnoreExistingComics(true);
				}
				if (commandLine.hasOption(CLI_IGNORE_MISSING)) {
					config.setIgnoreMissingComics(true);
				}

				wcm.downloadComics(webcomics);
			}
		} catch (ParseException e) {
			// invalid command line arguments
			System.out.println(e.getMessage());
			new HelpFormatter().printHelp("JDosage2 webcomic downloader", options);
		}
	}

}
