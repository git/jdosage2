/*
 * Copyright (c) 2012 Fran Jagić
 * All rights reserved.
 *
 * This file is part of JDosage2.
 *
 * JDosage2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * JDosage2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with JDosage2.  If not, see <http://www.gnu.org/licenses/>.
 */
package hr.fjagic.jdosage2.webcomic;

import hr.fjagic.jdosage2.Configuration;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.mail.internet.ContentDisposition;
import javax.mail.internet.ParseException;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.Connection;
import org.jsoup.Connection.Response;
import org.jsoup.helper.HttpConnection;
import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import freemarker.template.Template;
import freemarker.template.TemplateException;

/**
 * Webcomic model for JDosage2. Subclasses implement a comic download algorithm,
 * and contain additional fields for download progress tracking.
 *
 * <p> Webcomics are loaded from webcomic definition files.
 * {@link WebcomicManager} handles webcomic definition loading and comic
 * downloads.
 *
 * @author Fran Jagi&#263;
 * @since 0.5
 */
public abstract class Webcomic extends Properties implements Comparable<Webcomic> {

	private static final long serialVersionUID = -2239765252611641698L;

	/** Illegal filename characters */
	private static final char[] FILENAME_CHARS_BLACKLIST = new char[] { '|', '\\', '?', '*', '<', '"', ':', '>', '+',
			'[', ']', '/' };

	/** Illegal character placeholder */
	private static final char PLACEHOLDER = '_';

	private static final String NAME = "name";
	private static final String HOMEPAGE = "homepage";
	private static final String FILE_TEMPLATE = "fileTemplate";

	private static final String METADATA_NAME = "name";
	private static final String METADATA_IMAGE_URL = "imageUrl";
	private static final String METADATA_FILENAME = "filename";
	private static final String METADATA_FILE_EXTENSION = "fileExt";

	private static final String DEFAULT_FILE_TEMPLATE = "${name}/${filename}.${fileExt}";

	/** The logger for {@link Webcomic}s. */
	protected final Logger logger = LoggerFactory.getLogger(this.getClass());

	/** JDosage2 configuration */

	private Map<String, String> progress;

	private Template compiledTemplate;

	/**
	 * Gets the webcomic display name.
	 *
	 * @return the webcomic name
	 */
	public String getName() {
		return StringUtils.trimToNull(this.getProperty(NAME));
	}

	/**
	 * Gets the webcomic homepage. The homepage is optional and not used when
	 * downloading comics.
	 *
	 * @return the webcomic homepage
	 */
	public String getHomepage() {
		return StringUtils.trimToNull(this.getProperty(HOMEPAGE));
	}

	/**
	 * Gets the comic file template. When a comic is downloaded, file template
	 * is used to generate the path and filename of the image file.
	 *
	 * @see #processFileTemplate(Map)
	 * @return the file template
	 */
	public String getFileTemplate() {
		return ObjectUtils.firstNonNull(StringUtils.trimToNull(this.getProperty(FILE_TEMPLATE)), DEFAULT_FILE_TEMPLATE);
	}

	/**
	 * Returns fileTemplate compiled to {@link Template}.
	 *
	 * @return the compiled template
	 * @throws IOException
	 *             if a template parse error occured
	 */
	private Template compileTemplate() throws IOException {
		if (compiledTemplate == null) {
			compiledTemplate = new Template("fileTemplate", new StringReader(getFileTemplate()),
					new freemarker.template.Configuration());
		}
		return compiledTemplate;
	}

	/**
	 * Gets the download progress data, used for resuming webcomic downloads.
	 *
	 * <p> The webcomic implementation determines what data is stored, and
	 * {@link WebcomicManager} handles saving and restoring of progress data.
	 *
	 * @return the progress data
	 */
	public Map<String, String> getProgress() {
		if (progress == null) {
			progress = new HashMap<String, String>();
		}
		return progress;
	}

	/**
	 * Verifies that all required webcomic properties are set and all property
	 * values are valid.
	 *
	 * @throws ValidationException
	 *             if there is a problem with {@link Webcomic} configuration
	 */
	public void validate() throws ValidationException {
		if (getName() == null) {
			throw new ValidationException(NAME, NAME + " is required");
		}

		if (getHomepage() != null) {
			try {
				new URL(getHomepage());
			} catch (MalformedURLException e) {
				throw new ValidationException(HOMEPAGE, e);
			}
		}

		if (getFileTemplate() == null) {
			throw new ValidationException(FILE_TEMPLATE, FILE_TEMPLATE + " is required");
		} else {
			try {
				compileTemplate();
			} catch (IOException e) {
				throw new ValidationException(FILE_TEMPLATE, e);
			} catch (Exception e) {
				throw new ValidationException(FILE_TEMPLATE, e);
			}
		}
	}

	/**
	 * Downloads the next comic and updates progress data.
	 *
	 * @param config
	 *            JDosage2 configuration
	 * @return true if next comic is available, false if downloaded comic was
	 *         final
	 * @throws IOException
	 *             if a download error occurred
	 * @throws WebcomicException
	 *             if a webcomic processing error occurred
	 */
	public abstract boolean downloadNext(Configuration config) throws IOException, WebcomicException;

	/**
	 * Returns true if progress should be reset when download is finished. This
	 * is true if new downloads start from a fixed point (e.g. from the final
	 * comic going backwards). Default implementation returns false.
	 *
	 * @return the reset flag value
	 */
	protected boolean isResetWhenFinished() {
		return false;
	}

	/**
	 * Returns decoded url.
	 *
	 * @see URLDecoder#decode(String, String)
	 *
	 * @param url
	 *            url to decode
	 * @return decoded url
	 */
	protected String decodeUrl(String url) {
		try {
			return URLDecoder.decode(url, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			logger.warn("Error decoding url " + url, e);
			return url;
		}
	}

	/**
	 * Decodes HTML entites, trims the string and replaces illegal filename
	 * characters with underscore. Illegal filename characters are |, \, ?, *,
	 * <, ", :, >, +, [, ] and /. Concurrent whitespace characters are replaced
	 * with a single space.
	 *
	 * @param s
	 *            string to decode and clean
	 * @return decoded string
	 */
	protected String decodeHtml(String s) {
		if (s == null) {
			return null;
		}

		String decoded = StringEscapeUtils.unescapeHtml4(s);
		String trimmed = StringUtils.trim(decoded.replaceAll("\\s+", " "));
		String replaced = StringUtils.replaceChars(trimmed, String.valueOf(FILENAME_CHARS_BLACKLIST),
				StringUtils.repeat(PLACEHOLDER, FILENAME_CHARS_BLACKLIST.length));

		return replaced;
	}

	/**
	 * Applies comic metadata values to the file template.
	 *
	 * @param metadata
	 *            the comic metadata
	 * @return rendered file template
	 * @throws WebcomicException
	 *             if an error occurs while rendering file template
	 */
	private String processFileTemplate(Map<String, String> metadata) throws WebcomicException {
		logger.trace("Processing file template, metadata: {}", metadata);

		try {
			Template template = compileTemplate();
			StringWriter writer = new StringWriter();
			template.process(metadata, writer);
			String s = FilenameUtils.normalize(writer.toString());

			logger.trace("Rendered template: {}", s);

			return s;
		} catch (IOException e) {
			throw new WebcomicException("Error parsing template " + getFileTemplate() + " with metadata " + metadata, e);
		} catch (TemplateException e) {
			throw new WebcomicException("Error rendering template " + getFileTemplate() + " with metadata " + metadata,
					e);
		} catch (Exception e) {
			throw new WebcomicException("Error rendering template " + getFileTemplate() + " with metadata " + metadata,
					e);
		}
	}

	/**
	 * Downloads and parses webpage with the given url.
	 *
	 * @param url
	 *            webpage to download
	 * @param config
	 *            JDosage2 configuration
	 * @return web document
	 * @throws IOException
	 *             if a download error occurs
	 */
	protected Document downloadWebpage(String url, Configuration config) throws IOException {
		Connection connection = HttpConnection.connect(url).timeout(config.getConnectionTimeout() * 1000)
				.userAgent(config.getUserAgent());

		Document document = null;
		for (int i = 0; i < config.getConnectionRetries(); i++) {
			try {
				document = connection.get();
				break;
			} catch (IOException e) {
				if (i < config.getConnectionRetries() - 1) {
					logger.debug("Error downloading " + url + ", retrying", e);
				} else {
					throw e;
				}
			}
		}
		return document;
	}

	/**
	 * Downloads the comic image and saves it to the target file. Target file is
	 * generated by using metadata to render the file template, and resolving it
	 * relative to the download directory. If the target file already exists,
	 * download is skipped and the file is not overwritten.
	 *
	 * <p> Adds the following common metadata properties before rendering the
	 * file template:
	 *
	 * <table><thead><tr><th>Name</th> <th>Value</th> </tr> </thead><tbody>
	 *
	 * <tr><td>name</td> <td>name of the webcomic</td></tr>
	 *
	 * <tr> <td>imageUrl<sup>1</sup></td><td>url of the comic image</td> </tr>
	 *
	 * <tr> <td>filename</td> <td>filename of the comic image, without file
	 * extension</td> </tr>
	 *
	 * <tr> <td>fileExt</td> <td>comic image file extension</td> </tr></tbody>
	 * </table>
	 *
	 * <p> 1. url decoded, see {@link Webcomic#decodeUrl(String)}
	 *
	 * @param metadata
	 *            comic metadata
	 * @param imageUrl
	 *            url of the image to download
	 * @param referrer
	 *            HTTP referer header
	 * @param config
	 *            JDosage2 configuration
	 * @return true if the file was saved, false if it already existed
	 * @throws IOException
	 *             if a download or write error occurs
	 * @throws WebcomicException
	 *             if the downloaded file is not an image
	 */
	protected boolean downloadComic(Map<String, String> metadata, String imageUrl, String referrer, Configuration config)
			throws IOException, WebcomicException {
		try {
			// add common metadata properties
			Map<String, String> newMetadata = new HashMap<String, String>(metadata);
			String decodedUrl = decodeUrl(imageUrl);
			newMetadata.put(METADATA_NAME, getName());
			newMetadata.put(METADATA_IMAGE_URL, decodedUrl);
			newMetadata.put(METADATA_FILENAME, FilenameUtils.getBaseName(decodedUrl));
			newMetadata.put(METADATA_FILE_EXTENSION, FilenameUtils.getExtension(decodedUrl));

			// check if target file exists
			WebcomicException templateError = null;
			File targetFile = null;
			try {
				targetFile = new File(config.getDownloadDir(), processFileTemplate(newMetadata));
				logger.trace("Downloading {} to {}", imageUrl, targetFile);
				if (targetFile.exists()) {
					logger.info("New {} skipped because {} already exists", getName(), targetFile);
					return false;
				}
			} catch (WebcomicException e) {
				// on fail, try resolving template with content disposition
				// filename
				templateError = e;
			}

			// download image
			Connection connection = HttpConnection.connect(imageUrl).timeout(config.getConnectionTimeout() * 1000)
					.userAgent(config.getUserAgent()).ignoreContentType(true);
			if (referrer != null) {
				connection.referrer(referrer);
			}

			Response response = null;
			for (int i = 0; i < config.getConnectionRetries(); i++) {
				try {
					response = connection.execute();
					break;
				} catch (IOException e) {
					if (i < config.getConnectionRetries() - 1) {
						logger.debug("Error downloading " + imageUrl + ", retrying", e);
					} else {
						// last try
						throw e;
					}
				}
			}

			if (response == null) {
				throw new NullPointerException("Error downloading " + imageUrl + ", response is null");
			}

			// check response mime-type
			if (response.contentType() != null && !response.contentType().startsWith("image")) {
				throw new WebcomicException(imageUrl + " is " + response.contentType() + ", not an image");
			}

			// if response contains Content-Disposition header, update image
			// filename
			if (response.hasHeader("Content-Disposition")) {
				String contentDisposition = response.header("Content-Disposition");
				logger.trace("Found response header Content-Disposition: {}", contentDisposition);
				try {
					ContentDisposition cd = new ContentDisposition(contentDisposition);
					String filename = cd.getParameter("filename");
					if (StringUtils.isNotBlank(filename)) {
						newMetadata.put(METADATA_FILENAME, FilenameUtils.getBaseName(filename));
						newMetadata.put(METADATA_FILE_EXTENSION, FilenameUtils.getExtension(filename));
						targetFile = new File(config.getDownloadDir(), processFileTemplate(newMetadata));
						logger.trace("Filename changed, downloading {} to {}", imageUrl, targetFile);
						if (targetFile.exists()) {
							logger.info("New {} skipped because {} already exists", getName(), targetFile);
							return false;
						}
					}
				} catch (ParseException e) {
					logger.warn("Error parsing response header Content-Disposition: " + contentDisposition, e);
				}
			} else {
				if (templateError != null) {
					// no filename in content disposition, original error
					// is relevant
					throw templateError;
				}
			}

			// write image to temp file, if ok rename temp file
			File tempFile = new File(targetFile.getParentFile(), targetFile.getName() + ".part");
			Thread cleanupThread = new Thread(new CleanupRunnable(tempFile));
			Runtime.getRuntime().addShutdownHook(cleanupThread);

			FileUtils.writeByteArrayToFile(tempFile, response.bodyAsBytes());
			tempFile.renameTo(targetFile);

			Runtime.getRuntime().removeShutdownHook(cleanupThread);

			logger.info("New {} downloaded to {}", getName(), targetFile);
			return true;
		} catch (IOException e) {
			throw e;
		} catch (WebcomicException e) {
			throw e;
		} catch (Exception e) {
			throw new WebcomicException("Error downloading " + imageUrl, e);
		}
	}

	@Override
	public int compareTo(Webcomic o) {
		return this.getName().compareTo(o.getName());
	}

	/**
	 * Deletes temp file if disk write is interrupted by shutdown.
	 */
	private class CleanupRunnable implements Runnable {

		private File file;

		public CleanupRunnable(File file) {
			this.file = file;
		}

		@Override
		public void run() {
			if (FileUtils.deleteQuietly(file)) {
				Webcomic.this.logger.trace("Deleted incomplete file {}", file);
			}
		}

	}
}
