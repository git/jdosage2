/* 
 * Copyright (c) 2012 Fran Jagić
 * All rights reserved.
 * 
 * This file is part of JDosage2.
 * 
 * JDosage2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * JDosage2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with JDosage2.  If not, see <http://www.gnu.org/licenses/>.
 */
package hr.fjagic.jdosage2.webcomic;

/**
 * Exception thrown by {@link Webcomic#validate()} if webcomic validation fails.
 * 
 * @author Fran Jagi&#263;
 * @since 0.5
 */
public class ValidationException extends WebcomicException {

	private static final long serialVersionUID = -2996891339673999251L;

	private String field;

	/**
	 * Constructs a new {@link ValidationException} for the specified field with
	 * the specified detail message.
	 * 
	 * @param field
	 *            field that failed validation
	 * @param message
	 *            the detail message
	 */
	public ValidationException(String field, String message) {
		super(message);
		this.field = field;
	}

	/**
	 * Constructs a new {@link ValidationException} for the specified field with
	 * the specified detail message and cause.
	 * 
	 * @param field
	 *            field that failed validation
	 * @param message
	 *            the detail message
	 * @param cause
	 *            cause of validation fail
	 */
	public ValidationException(String field, String message, Throwable cause) {
		super(message, cause);
		this.field = field;
	}

	/**
	 * Constructs a new {@link ValidationException} for the specified field with
	 * the specified cause.
	 * 
	 * @param field
	 *            field that failed validation
	 * @param cause
	 *            cause of validation fail
	 */
	public ValidationException(String field, Throwable cause) {
		super(cause);
		this.field = field;
	}

	/**
	 * Gets the name of the field that failed validation.
	 * 
	 * @return field name
	 */
	public String getField() {
		return field;
	}

}
