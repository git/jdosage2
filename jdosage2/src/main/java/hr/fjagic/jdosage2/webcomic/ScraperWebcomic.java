/* 
 * Copyright (c) 2012 Fran Jagić
 * All rights reserved.
 * 
 * This file is part of JDosage2.
 * 
 * JDosage2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * JDosage2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with JDosage2.  If not, see <http://www.gnu.org/licenses/>.
 */
package hr.fjagic.jdosage2.webcomic;

import hr.fjagic.jdosage2.Configuration;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

/**
 * Scrapes webpage HTML for comic images and links.
 * {@link #downloadNext(Configuration)} algorithm works as follows:
 * 
 * <ol> <li>Downloads the current comic webpage.</li>
 * 
 * <li>Finds images/links matching the comic selector, and downloads
 * comics.</li>
 * 
 * <li>Finds the link matching nextPage selector and updates download progress
 * with the next url.</li>
 * 
 * <li>If downloading from the last page backwards, returns false only when a
 * comic file already exists or no next page link is found. If downloading from
 * the first page forwards, returns false only when no next page link is
 * found.</li>
 * 
 * <p> Metadata for {@link ScraperWebcomic} file templates contains the
 * following additional properties:
 * 
 * <table><thead><tr><th>Name</th> <th>Value</th> </tr> </thead><tbody>
 * 
 * <tr><td>pageUrl<sup>1</sup></td> <td>url of the current webpage</td></tr>
 * 
 * <tr><td>pageTitle<sup>2</sup></td> <td>HTML title of the current
 * webpage</td></tr>
 * 
 * <tr> <td>imageText<sup>2</sup></td> <td>alt text of the comic image, or link
 * text if the image is linked</td> </tr> </tbody> </table>
 * 
 * <p> 1. url decoded, see {@link Webcomic#decodeUrl(String)}<br /> 2. html
 * decoded, see {@link Webcomic#decodeHtml(String)}
 * 
 * @author Fran Jagi&#263;
 * @since 0.5
 */
public class ScraperWebcomic extends Webcomic {

	private static final long serialVersionUID = -4488900918904518616L;

	private static final String FIRST_PAGE = "firstPage";
	private static final String LAST_PAGE = "lastPage";
	private static final String COMIC = "comic";
	private static final String NEXT_PAGE = "nextPage";
	private static final String PROGRESS_PAGE = "page";

	private static final String METADATA_PAGE_URL = "pageUrl";
	private static final String METADATA_PAGE_TITLE = "pageTitle";
	private static final String METADATA_IMAGE_TEXT = "imageText";

	/**
	 * Gets the url of the first comic webpage. If firstPage is set, new
	 * downloads start from the last downloaded comic, and
	 * {@link #isResetWhenFinished()} returns false. If lastPage is set,
	 * firstPage value is ignored.
	 * 
	 * @return the first comic url
	 */
	public String getFirstPage() {
		return StringUtils.trimToNull(this.getProperty(FIRST_PAGE));
	}

	/**
	 * Gets the url of the last comic webpage. If lastPage is set, new downloads
	 * start from the last page, and {@link #isResetWhenFinished()} returns
	 * true.
	 * 
	 * @return the last comic url
	 */
	public String getLastPage() {
		return StringUtils.trimToNull(this.getProperty(LAST_PAGE));
	}

	/**
	 * Gets the {@link Selector} for comic images.
	 * 
	 * @return the comic selector
	 */
	public String getComic() {
		return StringUtils.trimToNull(this.getProperty(COMIC));
	}

	/**
	 * Gets the {@link Selector} for the next webcomic page link.
	 * 
	 * @return the next link selector
	 */
	public String getNextPage() {
		return StringUtils.trimToNull(this.getProperty(NEXT_PAGE));
	}

	/**
	 * Gets the current webcomic page.
	 * 
	 * @return the current webcomic page
	 */
	private String getCurrentPage() {
		return StringUtils.trimToNull(getProgress().get(PROGRESS_PAGE));
	}

	/**
	 * Sets the current webcomic page.
	 * 
	 * @param currentPage
	 *            the new webcomic page
	 */
	private void setCurrentPage(String currentPage) {
		getProgress().put(PROGRESS_PAGE, currentPage);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Webcomic#validate()
	 */
	@Override
	public void validate() throws ValidationException {
		super.validate();

		if (getFirstPage() == null && getLastPage() == null) {
			throw new ValidationException(LAST_PAGE, LAST_PAGE + " is required");
		} else if (getLastPage() != null) {
			try {
				new URL(getLastPage());
			} catch (MalformedURLException e) {
				throw new ValidationException(LAST_PAGE, e);
			}
		} else { // firstPage != null
			try {
				new URL(getFirstPage());
			} catch (MalformedURLException e) {
				throw new ValidationException(FIRST_PAGE, e);
			}
		}

		if (getComic() == null) {
			throw new ValidationException(COMIC, COMIC + " is required");
		} else {
			try {
				new Document("").select(getComic());
			} catch (Exception e) {
				throw new ValidationException(COMIC, e);
			}
		}

		if (getNextPage() == null) {
			throw new ValidationException(NEXT_PAGE, NEXT_PAGE + " is required");
		} else {
			try {
				new Document("").select(getNextPage());
			} catch (Exception e) {
				throw new ValidationException(NEXT_PAGE, e);
			}
		}

		if (getCurrentPage() != null) {
			try {
				new URL(getCurrentPage());
			} catch (MalformedURLException e) {
				throw new ValidationException(PROGRESS_PAGE, "Progress data is invalid", e);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see Webcomic#downloadNext(java.lang.String)
	 */
	public boolean downloadNext(Configuration config) throws IOException, WebcomicException {
		String pageUrl = ObjectUtils.firstNonNull(getCurrentPage(), getLastPage(), getFirstPage());
		logger.debug("Downloading next {} from {}", getName(), pageUrl);

		try {
			this.validate();

			// init metadata
			Map<String, String> metadata = new HashMap<String, String>();
			metadata.put(METADATA_PAGE_URL, decodeUrl(pageUrl));

			// downlad webpage and get base URL
			Document document = downloadWebpage(pageUrl, config);
			Elements baseTags = document.select("base[href]");
			if (!baseTags.isEmpty()) {
				document.setBaseUri(baseTags.first().absUrl("href"));
			}

			// read the title
			Elements titles = document.select("head title");
			if (!titles.isEmpty()) {
				Element title = titles.first();
				logger.trace("Found title: {}", title);
				metadata.put(METADATA_PAGE_TITLE, decodeHtml(title.ownText()));
			}

			// download all comics
			Elements comics = document.select(getComic());
			if (comics.isEmpty()) {
				if (config.isIgnoreMissingComics()) {
					logger.warn("No {} comics found in {}", getName(), pageUrl);
				} else {
					throw new WebcomicException("No " + getName() + " comics found in " + pageUrl);
				}
			}
			for (Element comic : comics) {
				logger.trace("Found comic: {}", comic);

				String imageUrl;
				String imageText;
				if ("img".equalsIgnoreCase(comic.tagName())) {
					// found image tag
					imageUrl = comic.absUrl("src");
					imageText = comic.attr("alt");
				} else if ("a".equalsIgnoreCase(comic.tagName())) {
					// found link tag
					imageUrl = comic.absUrl("href");
					imageText = comic.ownText();
				} else {
					throw new WebcomicException("Comic " + comic + " is not an image or a link");
				}

				metadata.put(METADATA_IMAGE_TEXT, decodeHtml(imageText));

				// download comic, stop if going backwards and file exists
				if (!downloadComic(metadata, imageUrl, pageUrl, config) && isResetWhenFinished()
						&& !config.isIgnoreExistingComics()) {
					logger.trace("Comic file exists, stopping");
					getProgress().clear();
					return false;
				}
			}

			Elements nextPages = document.select(getNextPage());
			// stop if there is no next comic
			if (nextPages.isEmpty()) {
				logger.trace("Link to next page not found, stopping");
				if (isResetWhenFinished()) {
					getProgress().clear();
				}
				return false;
			} else {
				String nextUrl = null;
				// if multiple links are found, they must be equal
				for (Element nextPage : nextPages) {
					if (!"a".equalsIgnoreCase(nextPage.tagName())) {
						throw new WebcomicException("Next page " + nextPage + " is not a link");
					}

					logger.trace("Found next page link: {}", nextPage);

					String newUrl = nextPage.absUrl("href");
					if (nextUrl == null) {
						nextUrl = newUrl;
					} else if (!nextUrl.equals(newUrl)) {
						throw new WebcomicException("More than one next page link found: " + nextPages);
					}
				}

				if (nextUrl.equals(pageUrl)) {
					logger.warn("{} next page url {} is same as the current page", getName(), nextUrl);
					return false;
				}

				setCurrentPage(nextUrl.toString());
				return true;
			}
		} catch (IOException e) {
			throw e;
		} catch (WebcomicException e) {
			throw e;
		} catch (Exception e) {
			throw new WebcomicException("Error downloading " + getName() + " from " + getCurrentPage(), e);
		}
	}

	@Override
	protected boolean isResetWhenFinished() {
		return getLastPage() != null;
	}
}
