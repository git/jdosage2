/* 
 * Copyright (c) 2012 Fran Jagić
 * All rights reserved.
 * 
 * This file is part of JDosage2.
 * 
 * JDosage2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * JDosage2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with JDosage2.  If not, see <http://www.gnu.org/licenses/>.
 */
package hr.fjagic.jdosage2;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.qos.logback.classic.Level;

/**
 * Configuration for JDosage2.
 * 
 * @author Fran Jagi&#263;
 * @since 0.5
 */
// TODO override configuration with system properties
public class Configuration extends Properties {

	private static final long serialVersionUID = -5532872512283240474L;

	private static Logger logger = LoggerFactory.getLogger(Configuration.class);

	private static Configuration instance;

	// static configuration values
	/**
	 * Directory containing configuration, progress file and webcomic
	 * definitions
	 */
	private static final File BASE_DIR = new File(System.getProperty("user.home"), ".jdosage2");

	/** File containing configuration */
	private static final File CONFIGURATION_FILE = new File(BASE_DIR, "config.properties");

	private static final File PROGRESS_FILE = new File(BASE_DIR, "progress.properties");

	private static final File DEFINITIONS_DIR = new File(BASE_DIR, "webcomics");

	private static final String SCRAPERWEBCOMIC_EXTENSION = "wcd";

	// TODO replace with a specific agent
	private static final String USER_AGENT = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:16.0) Gecko/20100101 Firefox/16.0";

	// user configuration property names
	private static final String DOWNLOAD_DIR = "download.dir";

	private static final String CONNECTION_TIMEOUT = "connection.timeout";

	private static final String CONNECTION_RETRIES = "connection.retries";

	private static final String LOG_LEVEL_CONSOLE = "log.level.console";

	private static final String LOG_LEVEL_FILE = "log.level.file";

	// runtime configuration
	private boolean ignoreExistingComics = false;

	private boolean ignoreMissingComics = false;

	private Configuration() {
		// initialize with default properties
		super(getDefaults());

		// load configuration
		if (CONFIGURATION_FILE.exists()) {
			try {
				FileReader reader = new FileReader(CONFIGURATION_FILE);
				this.load(reader);
				IOUtils.closeQuietly(reader);
			} catch (IOException e) {
				logger.error("Error reading configuration file", e);
			} catch (Exception e) {
				logger.error("Error loading configuration", e);
			}
		} else {
			try {
				BASE_DIR.mkdirs();
				this.defaults.store(new FileWriter(CONFIGURATION_FILE), "JDosage2 configuration");
				DEFINITIONS_DIR.mkdirs();
				logger.trace("Default configuration file {} created", CONFIGURATION_FILE);
			} catch (IOException e) {
				logger.error("Error creating default configuration", e);
			} catch (Exception e) {
				logger.error("Error saving configuration", e);
			}
		}
	}

	private static Properties getDefaults() {
		Properties defaults = new Properties();
		File downloadDir = new File(System.getProperty("user.home"), "Webcomics");
		defaults.setProperty(DOWNLOAD_DIR, downloadDir.getPath());
		defaults.setProperty(CONNECTION_TIMEOUT, "10");
		defaults.setProperty(CONNECTION_RETRIES, "3");
		defaults.setProperty(LOG_LEVEL_CONSOLE, Level.INFO.toString());
		defaults.setProperty(LOG_LEVEL_FILE, Level.DEBUG.toString());
		return defaults;
	}

	/**
	 * Returns JDosage2 configuration.
	 * 
	 * @return configuration
	 */
	public static Configuration getInstance() {
		if (instance == null) {
			instance = new Configuration();
		}
		return instance;
	}

	/**
	 * Returns the properties file containing webcomic download progress.
	 * 
	 * @return progres file
	 */
	public File getProgressFile() {
		return PROGRESS_FILE;
	}

	/**
	 * Returns the directory containing webcomic definitions.
	 * 
	 * @return webcomic definitions directory
	 */
	public File getDefinitionsDir() {
		return DEFINITIONS_DIR;
	}

	/**
	 * Returns the file extension of {@link ScraperWebcomic} definition files.
	 * 
	 * @return webcomic definition file extension
	 */
	public String getDefinitionExtension() {
		return SCRAPERWEBCOMIC_EXTENSION;
	}

	/**
	 * Returns the user agent for HTTP requests
	 * 
	 * @return user agent
	 */
	public String getUserAgent() {
		return USER_AGENT;
	}

	/**
	 * Returns webcomic download directory. If not set, returns default
	 * directory, ./Webcomics.
	 * 
	 * @return download directory
	 */
	public File getDownloadDir() {
		return new File(getProperty(DOWNLOAD_DIR));
	}

	/**
	 * Returns connection timeout, used when downloading files. If not set,
	 * returns default timeout, 10.
	 * 
	 * @return timeout in seconds
	 */
	public int getConnectionTimeout() {
		return NumberUtils.toInt(getProperty(CONNECTION_TIMEOUT), 10);
	}

	/**
	 * Returns the number of download retries on timeout or connection error. If
	 * not set, returns default value, 3.
	 * 
	 * @return download retries
	 */
	public int getConnectionRetries() {
		return NumberUtils.toInt(getProperty(CONNECTION_RETRIES), 3);
	}

	/**
	 * Gets the {@link #ignoreExistingComics} flag. If true, downloads don't
	 * stop when an existing comic file is found. Default value is false.
	 * 
	 * @return flag value
	 */
	public boolean isIgnoreExistingComics() {
		return ignoreExistingComics;
	}

	/**
	 * Sets the {@link #ignoreExistingComics} flag.
	 * 
	 * @param ignoreExistingComics
	 *            new flag value
	 */
	public void setIgnoreExistingComics(boolean ignoreExistingComics) {
		this.ignoreExistingComics = ignoreExistingComics;
	}

	/**
	 * Gets the {@link #ignoreMissingComics} flag. If true, downloads don't stop
	 * when a webpage contains no comic images. Default value is false.
	 * 
	 * @return flag value
	 */
	public boolean isIgnoreMissingComics() {
		return ignoreMissingComics;
	}

	/**
	 * Sets the {@link #ignoreMissingComics} flag.
	 * 
	 * @param ignoreMissingComics
	 *            new flag value
	 */
	public void setIgnoreMissingComics(boolean ignoreMissingComics) {
		this.ignoreMissingComics = ignoreMissingComics;
	}

}
