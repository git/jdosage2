/* 
 * Copyright (c) 2012 Fran Jagić
 * All rights reserved.
 * 
 * This file is part of JDosage2.
 * 
 * JDosage2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * JDosage2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with JDosage2.  If not, see <http://www.gnu.org/licenses/>.
 */
package hr.fjagic.jdosage2;

import hr.fjagic.jdosage2.webcomic.ScraperWebcomic;
import hr.fjagic.jdosage2.webcomic.ValidationException;
import hr.fjagic.jdosage2.webcomic.Webcomic;
import hr.fjagic.jdosage2.webcomic.WebcomicException;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOCase;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.filefilter.FileFilterUtils;
import org.apache.commons.io.filefilter.IOFileFilter;
import org.apache.commons.io.filefilter.NameFileFilter;
import org.apache.commons.io.filefilter.SuffixFileFilter;
import org.apache.commons.io.filefilter.TrueFileFilter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * WebcomicManager loads webcomic definitions and manages comic downloads.
 * 
 * @author Fran Jagi&#263;
 * @since 0.5
 */
public class WebcomicManager {

	private static final Logger logger = LoggerFactory.getLogger(WebcomicManager.class);

	private Configuration config;

	private Properties progress;

	/**
	 * Webcomic with download in progress, used for shutdown cleanup
	 */
	private Webcomic currentWebcomic;

	/**
	 * Download count for {@link #currentWebcomic}. If < 0, count was already
	 * logged
	 */
	private int currentCounter = -1;

	/**
	 * Creates a new {@link WebcomicManager}.
	 * 
	 * @param config
	 *            JDosage2 configuration
	 */
	public WebcomicManager(Configuration config) {
		this.config = config;

		// save progress on shutdown
		Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {

			@Override
			public void run() {
				// update progress data for interrupted download
				if (currentWebcomic != null) {
					if (currentCounter >= 0) {
						logger.info("{}: {} comic pages downloaded, stopped by user", currentWebcomic.getName(),
								currentCounter);
					}
					WebcomicManager.this.updateProgress(currentWebcomic);
				}

				// save download progress
				WebcomicManager.this.saveProgress();
			}

		}));
	}

	/**
	 * Loads and returns all webcomics from the definition files.
	 * 
	 * <p> Webcomic definitions directory and all its subdirectories are
	 * searched.
	 * 
	 * @see Configuration#getDefinitionsDir()
	 * @return loaded webcomics
	 */
	public Collection<Webcomic> getAllWebcomics() {
		return getWebcomics(TrueFileFilter.INSTANCE);
	}

	/**
	 * Loads and returns webcomics from the definition files with the given
	 * filenames.
	 * 
	 * <p> Webcomic definitions directory and all its subdirectories are
	 * searched. Filenames are case insensitive.
	 * 
	 * @see Configuration#getDefinitionsDir()
	 * @param filenames
	 *            filenames of webcomic definition files, without file extension
	 * @return loaded webcomics
	 */
	public Collection<Webcomic> getWebcomicsByFilename(String[] filenames) {
		if (filenames == null || filenames.length == 0) {
			return Collections.emptyList();
		}

		IOFileFilter fileFilter = new WebcomicFilenameFilter(filenames, config.getDefinitionExtension());
		return getWebcomics(fileFilter);
	}

	/**
	 * Loads and returns webcomics from the definition files with the given
	 * group.
	 * 
	 * <p> Webcomic group is a subdirectory of the webcomic definitions
	 * directory, of any level. Groups are case insensitive.
	 * 
	 * @see Configuration#getDefinitionsDir()
	 * @param groups
	 *            groups of webcomic definition files
	 * @return loaded webcomics
	 */
	public Collection<Webcomic> getWebcomicsByGroup(String[] groups) {
		if (groups == null || groups.length == 0) {
			return Collections.emptyList();
		}

		IOFileFilter groupFilter = new WebcomicGroupFilter(groups, config.getDefinitionsDir(),
				config.getDefinitionExtension());
		return getWebcomics(FileFilterUtils.and(groupFilter,
				FileFilterUtils.suffixFileFilter("." + config.getDefinitionExtension(), IOCase.INSENSITIVE)));
	}

	/**
	 * Loads and returns webcomics from the definition files matching the file
	 * and directory filter.
	 * 
	 * @param fileFilter
	 *            file filter
	 * @param dirFilter
	 *            directory filter
	 * @return loaded webcomics
	 */
	private Collection<Webcomic> getWebcomics(IOFileFilter fileFilter) {
		if (!config.getDefinitionsDir().exists()) {
			logger.warn("Webcomic definitions directory {} doesn't exist", config.getDefinitionsDir());
			return Collections.emptyList();
		}

		Collection<File> definitions = FileUtils.listFiles(config.getDefinitionsDir(), fileFilter,
				TrueFileFilter.INSTANCE);

		if (definitions.isEmpty()) {
			logger.info("No matching webcomic definitions found");
			return Collections.emptyList();
		}

		// TODO equality determined by Properties.equals(), change to definition
		// file
		Set<Webcomic> webcomics = new HashSet<Webcomic>(definitions.size());
		for (File definition : definitions) {
			try {
				Webcomic webcomic = loadWebcomic(definition);
				webcomics.add(webcomic);
			} catch (IOException e) {
				logger.error("Error reading webcomic definition file " + definition.getName(), e);
			} catch (ValidationException e) {
				logger.error("Webcomic definition file " + definition.getName() + " is invalid", e);
			} catch (Exception e) {
				logger.error("Error loading webcomic definition file " + definition.getName(), e);
			}
		}

		return webcomics;
	}

	/**
	 * Loads a webcomic from the definition file.
	 * 
	 * @param definition
	 *            webcomic definition file
	 * @return webcomic
	 * @throws IOException
	 *             if a file read error occured
	 * @throws ValidationException
	 *             if the webcomic failed validation
	 */
	private Webcomic loadWebcomic(File definition) throws IOException, ValidationException {
		logger.trace("Loading webcomic definition file {}", definition);

		ScraperWebcomic webcomic = new ScraperWebcomic();
		FileReader reader = new FileReader(definition);
		webcomic.load(reader);
		IOUtils.closeQuietly(reader);

		webcomic.validate();

		// inject progress
		String prefix = getProgressPrefix(webcomic);
		for (Entry<Object, Object> entry : getProgress().entrySet()) {
			if (((String) entry.getKey()).startsWith(prefix)) {
				String key = ((String) entry.getKey()).substring(prefix.length());
				String value = (String) entry.getValue();
				webcomic.getProgress().put(key, value);
			}
		}

		logger.debug("Loaded {}, progress: {}", webcomic.getName(), webcomic.getProgress());
		return webcomic;
	}

	/**
	 * Returns prefix for webcomic's progress data in the saved properties file.
	 * 
	 * @param webcomic
	 *            webcomic for prefix generation
	 * @return prefix
	 */
	private String getProgressPrefix(Webcomic webcomic) {
		return webcomic.getName().replaceAll("\\s+", "_") + ".";
	}

	/**
	 * Download comics for all given webcomics.
	 * 
	 * @param webcomics
	 *            webcomics to download
	 */
	public void downloadComics(Collection<Webcomic> webcomics) {
		for (Webcomic webcomic : webcomics) {
			downloadComics(webcomic);
		}
	}

	/**
	 * Downloads comics for the given webcomic.
	 * 
	 * @param webcomic
	 *            webcomic to download
	 */
	private void downloadComics(Webcomic webcomic) {
		logger.info("Downloading {} to {}", webcomic.getName(), config.getDownloadDir());

		currentWebcomic = webcomic;
		currentCounter = 0;
		try {
			do {
				++currentCounter;
				logger.trace(StringUtils.repeat('*', 60));
			} while (webcomic.downloadNext(config));
			logger.info("{}: {} comic pages downloaded", webcomic.getName(), currentCounter);
		} catch (IOException e) {
			logger.warn(webcomic.getName() + ": " + currentCounter + " comic pages downloaded, finished with error", e);
		} catch (WebcomicException e) {
			logger.warn(webcomic.getName() + ": " + currentCounter + " comic pages downloaded, finished with error", e);
		} catch (Exception e) {
			logger.warn(webcomic.getName() + ": " + currentCounter + " comic pages downloaded, finished with error", e);
		} finally {
			currentCounter = -1;
			updateProgress(currentWebcomic);
			currentWebcomic = null;
			logger.info(StringUtils.repeat('-', 60));
		}
	}

	private Properties getProgress() {
		if (progress == null) {
			progress = new Properties();
			// load progress from file
			if (config.getProgressFile().exists()) {
				try {
					FileReader reader = new FileReader(config.getProgressFile());
					progress.load(reader);
					IOUtils.closeQuietly(reader);
				} catch (IOException e) {
					logger.error("Error reading progress file");
				} catch (Exception e) {
					logger.error("Error loading progress data");
				}
			}
		}
		return progress;
	}

	/**
	 * Updates progress data with the new download progress of the given
	 * webcomic.
	 * 
	 * @param webcomic
	 *            webcomic for progress update
	 */
	private void updateProgress(Webcomic webcomic) {
		String prefix = getProgressPrefix(webcomic);

		// remove old progress
		for (Iterator<Entry<Object, Object>> it = getProgress().entrySet().iterator(); it.hasNext();) {
			Entry<Object, Object> entry = it.next();
			if (((String) entry.getKey()).startsWith(prefix)) {
				logger.trace("Progress entry {} removed ", entry);
				it.remove();
			}
		}

		// insert new progress
		for (Entry<String, String> entry : webcomic.getProgress().entrySet()) {
			logger.trace("Progress entry {}={} added", prefix + entry.getKey(), entry.getValue());
			getProgress().setProperty(prefix + entry.getKey(), entry.getValue());
		}
	}

	/**
	 * Saves progress data to properties file.
	 */
	private void saveProgress() {
		try {
			config.getProgressFile().getParentFile().mkdirs();
			FileWriter writer = new FileWriter(config.getProgressFile());
			getProgress().store(writer, "JDosage2 download progress");
			IOUtils.closeQuietly(writer);
			logger.trace("Progress saved to {}: {}", config.getProgressFile(), getProgress());
		} catch (IOException e) {
			logger.error("Error writing progress file", e);
		} catch (Exception e) {
			logger.error("Error saving progress", e);
		}

	}

	private static class WebcomicFilenameFilter extends NameFileFilter {

		private static final long serialVersionUID = 2205299523785016539L;

		public WebcomicFilenameFilter(String[] names, String extension) {
			super(addExtension(names, extension), IOCase.INSENSITIVE);
		}

		private static String[] addExtension(String[] names, String extension) {
			String[] fullNames = new String[names.length];
			for (int i = 0; i < names.length; i++) {
				fullNames[i] = names[i] + "." + extension;
			}
			return fullNames;
		}

	}

	private static class WebcomicGroupFilter implements IOFileFilter {

		private NameFileFilter groupFilter;

		private File rootDir;

		private SuffixFileFilter suffixFilter;

		public WebcomicGroupFilter(String[] groups, File rootDir, String extension) {
			this.groupFilter = new NameFileFilter(groups);
			this.rootDir = rootDir;
			this.suffixFilter = new SuffixFileFilter("." + extension);
		}

		@Override
		public boolean accept(File file) {
			if (!suffixFilter.accept(file)) {
				return false;
			}

			File dir = file.getParentFile();

			// parent directories are group names
			while (dir != null && !dir.equals(rootDir)) {
				if (groupFilter.accept(dir)) {
					return true;
				}

				dir = dir.getParentFile();
			}

			return false;
		}

		@Override
		public boolean accept(File dir, String name) {
			if (!suffixFilter.accept(dir, name)) {
				return false;
			}

			return accept(new File(dir, name));
		}

	}

}
